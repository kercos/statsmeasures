# StatsMeasures

Repository containing code related to statistical measures:
- Shannon entropy
- Conditional mutual information
- TSE 
- Synergy

# Installation
1. Create and activate python virtual environment, and upgrade pip
   - `python3 -m venv .venv`
   - `source .venv/bin/activate`
   - `pip install -U pip`
2. Install requirements
   - `pip install -r requirements.txt`
3. Run tests:
   - `python -m measures.test.tse_plot`
   - `python -m measures.test.compare_tse`