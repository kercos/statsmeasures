import numpy as np
from measures.jidt.entropy_gaussian import compute_entropy_gaussian
from utils.bipartitions import compute_bipartitions
from utils.jidt import initJVM, shutdownJVM
from measures.jidt.mi_kraskov import compute_mi_kraskov
from numpy.random import RandomState

def compute_integration_tse_gaussian(data):
    # TSE
    assert data.ndim == 2
    num_rows, num_cols = data.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    sum_individual_entropies = 0
    for j in range(num_cols):
       column_data = np.expand_dims(data[:, j], -1)
       sum_individual_entropies += compute_entropy_gaussian(column_data)

    overall_entropy = compute_entropy_gaussian(data)    
    return sum_individual_entropies - overall_entropy

def test_compute_integration_tse_gaussian(seed=None):
    initJVM()
    rs = RandomState(seed)
    data = rs.random_sample((500, 4))    
    mi = compute_integration_tse_gaussian(data)    
    print(mi)
    shutdownJVM()

if __name__ == "__main__":
    test_compute_integration_tse_gaussian(123)