"""
Transfer entropy on continuous data using Kraskov estimators
for various types of time series relationships.
see 
- https://github.com/jlizier/jidt/wiki/PythonExamples

"""
import jpype as jp
import numpy as np
import random
import math
import jpype as jp

from measures.utils.jidt import initJVM


def compute_transfer_entropy_kraskov(data1, data2, dest_only=False):
    
    pkg = "infodynamics.measures.continuous.kraskov"
    teCalcClass = jp.JPackage(pkg).TransferEntropyCalculatorKraskov
    teCalc = teCalcClass()
    
    if dest_only:
        # embed the destination only using the Ragwitz criteria:
        teCalc.setProperty(teCalcClass.PROP_AUTO_EMBED_METHOD, teCalcClass.AUTO_EMBED_METHOD_RAGWITZ_DEST_ONLY)
        # this requires also the following
        # Since we're only auto-embedding the destination, we supply
        # source embedding here (to overwrite the auto embeddings from above):
        teCalc.setProperty(teCalcClass.L_PROP_NAME, "1")
        teCalc.setProperty(teCalcClass.L_TAU_PROP_NAME, "1")
    else:
        # Set properties for auto-embedding of both source and destination
        #  using the Ragwitz criteria:
        #  a. Auto-embedding method
        teCalc.setProperty(teCalcClass.PROP_AUTO_EMBED_METHOD, teCalcClass.AUTO_EMBED_METHOD_RAGWITZ)    

    #  b. Search range for embedding dimension (k) and delay (tau)
    teCalc.setProperty(teCalcClass.PROP_K_SEARCH_MAX, "6")    
    teCalc.setProperty(teCalcClass.PROP_TAU_SEARCH_MAX, "6")

    # Compute TE from breath (column 1) to heart (column 0) 
    teCalc.setObservations(data1, data2)
    te = teCalc.computeAverageLocalOfObservations()

    return te

def compute_transfer_entropy_kraskov_reciprocal(data1, data2):
    return np.mean(
        [
            compute_transfer_entropy_kraskov(data1, data2),
            compute_transfer_entropy_kraskov(data2, data1)
        ]
    )
