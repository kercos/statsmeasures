"""
Transfer entropy on continuous data using Kraskov estimators
for various types of time series relationships.
"""
import jpype as jp
import numpy as np
from measures.utils.utils import discretize

def compute_transfer_entropy_kraskov(data1, data2, dest_only=False):
    # see https://github.com/jlizier/jidt/blob/master/demos/python/example9TeKraskovAutoEmbedding.py
    pkg = "infodynamics.measures.continuous.kraskov"
    teCalcClass = jp.JPackage(pkg).TransferEntropyCalculatorKraskov
    teCalc = teCalcClass()
    
    if dest_only:
        # embed the destination only using the Ragwitz criteria:
        teCalc.setProperty(teCalcClass.PROP_AUTO_EMBED_METHOD, teCalcClass.AUTO_EMBED_METHOD_RAGWITZ_DEST_ONLY)
        # this requires also the following
        # Since we're only auto-embedding the destination, we supply
        # source embedding here (to overwrite the auto embeddings from above):
        teCalc.setProperty(teCalcClass.L_PROP_NAME, "1")
        teCalc.setProperty(teCalcClass.L_TAU_PROP_NAME, "1")
    else:
        # Set properties for auto-embedding of both source and destination
        #  using the Ragwitz criteria:
        #  a. Auto-embedding method
        teCalc.setProperty(teCalcClass.PROP_AUTO_EMBED_METHOD, teCalcClass.AUTO_EMBED_METHOD_RAGWITZ)    

    #  b. Search range for embedding dimension (k) and delay (tau)
    teCalc.setProperty(teCalcClass.PROP_K_SEARCH_MAX, "6")    
    teCalc.setProperty(teCalcClass.PROP_TAU_SEARCH_MAX, "6")

    # Compute TE from breath (column 1) to heart (column 0) 
    teCalc.setObservations(data1, data2)
    te = teCalc.computeAverageLocalOfObservations()

    return te

def compute_transfer_entropy_kraskov_reciprocal(data1, data2):
    return np.mean(
        [
            compute_transfer_entropy_kraskov(data1, data2),
            compute_transfer_entropy_kraskov(data2, data1)
        ]
    )

def compute_transfer_entropy_discrete(data1, data2, delay=1, reciprocal=True, log=False,
                         local=False, bins=100, min_v=0., max_v=1.):
    """
    Calculate transfer entropy from 2D time series.
    :param brain_output: time series numpy array
    :param delay: lag between source and destination
    :param reciprocal: whether to calculate average TE in both directions
    :param log: whether to print intermediate results
    :param local: whether to calculate local entropy values
    """
    assert data1.shape == data2.shape
    calcClass = jp.JPackage("infodynamics.measures.discrete").TransferEntropyCalculatorDiscrete
    source = discretize(data1, bins, min_v, max_v).tolist()
    destination = discretize(data2, bins, min_v, max_v).tolist()
    calc = calcClass(bins,1)
    calc.initialise()
    calc.addObservations(source, destination)            
    
    te_src_dst = calc.computeAverageLocalOfObservations()
    te_src_dst = te_src_dst / np.log2(bins)

    if log:
        print('te_src_dst: {}'.format(te_src_dst))

    local_te = []
    if local:
        te_src_dst_local = calc.computeLocalOfPreviousObservations()
        local_te.append(te_src_dst_local)
    if not reciprocal:
        return te_src_dst
    
    calc.initialise()  # Re-initialise leaving the parameters the same
    calc.addObservations(destination, source)
    te_dst_src = calc.computeAverageLocalOfObservations()
    te_dst_src = te_dst_src / np.log2(bins)
    avg_te = np.mean([te_src_dst, te_dst_src])
    if log:
        print('te_dst_src: {}'.format(te_dst_src))
    if local:
        te_dst_src_local = calc.computeLocalOfPreviousObservations()
        local_te.append(te_dst_src_local)
        return avg_te, local_te
    return avg_te

