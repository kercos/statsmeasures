import jpype as jp
import numpy as np
from measures.utils.jidt import initJVM, shutdownJVM
from numpy.random import RandomState

def compute_entropy_jidt_gaussian(data, add_noise=False):
    # multi variate mutual information
    jp_kernel_pkg = jp.JPackage("infodynamics.measures.continuous.gaussian")
    entropy_calc = jp_kernel_pkg.EntropyCalculatorMultiVariateGaussian()
    
    if not add_noise:
        entropy_calc.setProperty("NOISE_LEVEL_TO_ADD", "0") # no noise for reproducibility
    
    entropy_calc.initialise(data.shape[1]) # number of joint variables
    
    entropy_calc.setObservations(
        jp.JArray(jp.JDouble, 2)(data)
    )
    
    result = entropy_calc.computeAverageLocalOfObservations()
    return result

def test_compute_entropy_jidt_kernel(seed=None):
    initJVM()
    rs = RandomState(seed)
    data = rs.random_sample((1000, 10))    
    # data = np.zeros((1000, 5))    
    entropy = compute_entropy_jidt_gaussian(data)    
    print(entropy)
    shutdownJVM()

if __name__ == "__main__":
    test_compute_entropy_jidt_kernel(123)