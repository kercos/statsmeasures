import jpype as jp
import numpy as np
from measures.utils.jidt import initJVM, shutdownJVM

def compute_cond_mi_kraskov(data1, data2, cond_data, add_noise=False):
    # multi variate conditional mutual information
    jp_kraskov_pkg = jp.JPackage("infodynamics.measures.continuous.kraskov")
    cond_mi_calc = jp_kraskov_pkg.ConditionalMutualInfoCalculatorMultiVariateKraskov1()
    
    if not add_noise:
        cond_mi_calc.setProperty("NOISE_LEVEL_TO_ADD", "0") # no noise for reproducibility
    
    cond_mi_calc.initialise(data1.shape[1], data2.shape[1], 1) # number of source and destination joint variables
    
    cond_mi_calc.setObservations(
        jp.JArray(jp.JDouble, 2)(data1), 
        jp.JArray(jp.JDouble, 2)(data2),
        jp.JArray(jp.JDouble, 2)(cond_data)
    )
    
    result = cond_mi_calc.computeAverageLocalOfObservations()
    return result

def test_compute_cond_mi():
    initJVM()
    data1 = np.random.random_sample((1000, 10))    
    # mi = compute_mi(data1, data1)    
    data2 = np.random.random_sample((1000, 10))
    # data2 = data1 + 0.5 * np.random.random_sample((1000, 10))
    cond_data = np.random.random_sample((1000,1))    
    mi = compute_cond_mi_kraskov(data1, data2, cond_data)    
    print(mi)
    shutdownJVM()

if __name__ == "__main__":
    test_compute_cond_mi()