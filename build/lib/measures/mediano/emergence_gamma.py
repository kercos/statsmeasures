'''
This measure is taken from:
Rosas*, Mediano*, et al. (2020). Reconciling emergences: An information-theoretic approach to identify causal emergence in multivariate data. PLoS Computational Biology, 16(12): e1008289. 
DOI: 10.1371/journal.pcbi.1008289
https://github.com/pmediano/ReconcilingEmergences
'''

import numpy.random as rn
from oct2py import Oct2Py
from measures.mediano.mediano_gamma import MedianoGamma
import numpy as np
    
def test(repetitions=1):
    rn.seed(123)
    for i in range(repetitions):
        for tau in [1,2,3,5,10,20]:
            X = rn.randn(100,4)
            V = rn.randn(100,1)
            oc = Oct2Py()
            oc.addpath('measures/mediano')
        
            psi_py = MedianoGamma(X,V, tau)
            psi_oc = oc.EmergenceGamma(X,V, tau)    

            if not np.allclose(psi_py, psi_oc):    
                print(f"Inconsistent results for tau = {tau}")
                print(psi_oc)
                print(psi_py)
                return
    print('Test OK')
    
    

if __name__ == "__main__":
    test()
    