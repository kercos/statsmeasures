from itertools import chain, combinations

def powerset(num_elements, remove_empty=False, remove_complete=False):
    """Returns all possible subset of a se with n elements

    Args:
        num_elements ([type]): [description]
        remove_empty (bool, optional): whether to remove empty set. Defaults to False.
        remove_complete (bool, optional): whether to remove complete set. Defaults to False.

    Returns:
        [type]: [description]
    """
    elements = list(range(num_elements))
    min_size = 1 if remove_empty else 0
    max_size = num_elements if remove_complete else num_elements + 1
    result = chain.from_iterable(combinations(elements, r) for r in range(min_size, max_size))
    return list(result)

def test_powerset(num=4, remove_empty=True, remove_complete=False):        
    ps = powerset(num, remove_empty, remove_complete)
    size = len(ps)
    exp_size = 2 ** num    
    if remove_empty:
        exp_size -= 1
    if remove_complete:
        exp_size -= 1
    assert exp_size == size
    print(ps)
    print(size)

if __name__ == "__main__":
    test_powerset()