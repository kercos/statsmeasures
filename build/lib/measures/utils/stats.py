import numpy as np
from itertools import combinations
from scipy.stats import friedmanchisquare, ranksums, kruskal, spearmanr, pearsonr

def interpret_observed_effect_size(effectSize, label):
    if label == 1: #####  Eta^2 OR Epsilon^2
        if effectSize <= 0.01:					
            return 'Very Small Effect'
        elif 0.01 < effectSize < 0.06:					
            return 'Small Effect'
        elif 0.06 <= effectSize < 0.14:					
            return 'Medium Effect'
        elif effectSize >= 0.14:
            return 'Large Effect'
    elif label == 2:				
        if effectSize < 0.1:					
            return 'Very Small Effect'
        elif 0.01 <= effectSize < 0.3:					
            return 'Small Effect'
        elif 0.3 <= effectSize < 0.5:					
            return 'Medium Effect'
        elif effectSize >= 0.5:
            return 'Large Effect'

def show_descriptive_statistics(data, label):
    print('M-' + label, ' = ', np.mean(data), ' SD-' + label, ' = ', np.std(data), '  Mdn-' + label, ' = ', np.median(data), \
        '  CI_95%-' + label + ' = ', [np.percentile(data, 2.5), np.percentile(data, 97.5)])

def kruskal_wallis(matrix, whichData, col_labels, random_seed=0, debug=False):
    num_cols = len(col_labels)
    assert matrix.shape[1] == num_cols
    bonferroni_correction = 0.05 / num_cols
    np.random.seed(random_seed) # reproducibility
    if debug:
        print('\n====================================',  whichData, '\n')
    [h, p] = kruskal(matrix[:, 0], matrix[:, 1], matrix[:, 2])
    eta_squared_effect_size = (h - matrix.shape[1] + 1)/((matrix.shape[0] * matrix.shape[1]) - matrix.shape[1])

    if debug:
        print(
            'Kruskal-Wallis Test -  ', whichData, 
            ':  H-statistic = ', h, 
            '  p = ', p, 
            '  eta^2 = ', eta_squared_effect_size, 
            '(', interpret_observed_effect_size(eta_squared_effect_size, 1), ')'
        )

    post_hoc_computation = p < bonferroni_correction
    
    post_hoc_stats = None
    if post_hoc_computation:
        post_hoc_stats = wilcoxon_ranksum(matrix, col_labels, debug)

    return h, p, eta_squared_effect_size, post_hoc_stats

def wilcoxon_ranksum(matrix, col_labels, debug):
    num_cols = len(col_labels)
    all_pairs = list(combinations(list(range(num_cols)), 2)) # all pairs between sim types (e.g, [(0, 1), (0, 2), (1, 2)] for 3 sim_types)
    post_hoc_stats = np.zeros((len(all_pairs),3)) # for each pair among sim_types (row) we have 3 stats: sW, pW, effectSize
    for p_index, (i,j) in enumerate(all_pairs):
        [sW, pW] = ranksums(matrix[:, i], matrix[:, j])
        effectSize = abs(sW/np.sqrt(matrix.shape[0]))
        post_hoc_stats[p_index] = [sW, pW, effectSize]
        if debug:
            print(col_labels[i], ' vs. ', col_labels[j], '  s = ', sW, '  p = ', pW, '  effect-size = ', effectSize, '(', \
                interpret_observed_effect_size(effectSize, 2), ')')
            show_descriptive_statistics(matrix[:, i], col_labels[i])
            show_descriptive_statistics(matrix[:, j], col_labels[j])					
    return post_hoc_stats