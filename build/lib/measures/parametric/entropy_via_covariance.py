import numpy as np
from numpy.random import RandomState
from measures.test import dataset
from measures.utils.utils import add_noise

def compute_entropy_via_coveriance(data):
    if data.ndim == 1:
        data = np.expand_dims(data, -1)
    num_nodes = data.shape[1]
    # if num_nodes == 1: # single column
    #     cov_det = np.var(data)
    # else:
    cov_matrix = np.cov(data, rowvar=False)  # num_nodes x num_nodes
    if cov_matrix.size == 1:
        cov_det =  np.asscalar(cov_matrix)
    else:
        cov_det = np.linalg.det(cov_matrix)
    
    log_arg = ((2 * np.pi * np.e) ** num_nodes) * cov_det
    assert log_arg > 0
    entropy = 0.5 * np.log(log_arg)
    return entropy

def test_entropy(seed=123):
    rs = RandomState(seed)
    # data, _ = dataset.random_normal(1000, 10, rs)
    data, _ = dataset.random_normal_constant(1000, 10, rs)    
    data = add_noise(data)
    h = compute_entropy_via_coveriance(data)
    print(h)

if __name__ == "__main__":
    test_entropy()