# HOW TO SUBMIT NEW VERSION
# 1. Change version below (line 15)
# 2. Update CHANGELOG.rst
# 3. RUN: python setup.py sdist bdist_wheel

import setuptools

setuptools.setup(
    name="statsmeasures",
    version="0.6.0",
    author="Federico Sangati, Soheil Keshmiri, Ekaterina Sangati",
    author_email="federico.sangati2@oist.jp, soheil.keshmiri@oist.jp, ekaterina.sangati@oist.jp",
    description="Statistical measure for simulation models",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/oist-ecsu/statsmeasures",  # where the package will be hosted
    # a list of all Python import packages that should be included in the distribution package
    # in this case it'll only be example_pkg
    # packages=setuptools.find_packages(exclude=['contrib', 'docs']),
    packages=setuptools.find_packages(),
    install_requires=[
        'numpy',
        'scipy',
        'seaborn'
    ],
    classifiers=[
        "Programming Language :: Python :: 3.7.3",  # compatible only with Python 3.7.3
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    include_package_data=True,
    package_data={'measures': ['infodynamics.jar']},
)