import pkg_resources
import os
import jpype as jp

def initJVM():
    jar_location = pkg_resources.resource_filename('measures', 'infodynamics.jar')
    if (not(os.path.isfile(jar_location))):
        exit("infodynamics.jar not found (expected at " + os.path.abspath(jar_location) + ") - are you running from demos/python?")			
    jp.startJVM(jp.getDefaultJVMPath(), "-Xmx1024M", "-ea", "-Djava.class.path=" + jar_location, convertStrings = False)   # convertStrings = False to silence the Warning while starting JVM 						

def shutdownJVM():
    jp.shutdownJVM()
