def compute_bipartitions(num_elements, sort_by_size=True):
    assert num_elements > 1

    def compute_bipartitions(collection):
        if len(collection) == 2:
            yield [[collection[0]], [collection[1]]]
            return
        first = collection[0]
        rest = collection[1:]
        # put `first` in its own subset 
        yield [[first]] + [rest]
        for rest_bp in compute_bipartitions(rest):
            # insert `first` in each of the subpartition's subsets
            for n, subset in enumerate(rest_bp):
                yield rest_bp[:n] + [[first] + subset] + rest_bp[n + 1:]


    elements = list(range(num_elements))
    bp = list(compute_bipartitions(elements))
    if sort_by_size:
        for pair in bp:
            # put smaller bipart first
            pair.sort(key=len)
    return bp  

def test_bipartitions(num=4):
    bp = compute_bipartitions(num, sort_by_size=True)
    size = len(bp)
    print('total size:', size)

    max_size_half = num // 2
    bp_dict = {s:[] for s in range(1, max_size_half+1)}
    for pair in bp:
        bp_dict[len(pair[0])].append(pair)
    bp_sized = list(bp_dict.values())

    for k, bp_size in enumerate(bp_sized,1):
        print(f'k = {k}')
        print('\n'.join(str(x) for x in bp_size))

if __name__ == "__main__":
    test_bipartitions(4)