from numpy.random import RandomState
import numpy as np

def add_noise(data, rs=None, noise_level=1e-5):
    if rs is None:
        rs = RandomState(None)
    noise = rs.normal(0, noise_level, data.shape) # try using white noise: gaussian with mean=0 var=1
    data = data + noise    
    return data

def random_int(random_state=None, size=None):
    if random_state is None:
        return np.random.randint(0, 2147483647, size)
    else:
        return random_state.randint(0, 2147483647, size)

def discretize(a, bins, min_v=0, max_v=1):
    a[a > max_v] = max_v
    a[a < min_v] = min_v
    bins = np.linspace(min_v, max_v, bins)
    return np.digitize(a, bins, right=True)
