# see https://quantcorner.wordpress.com/2018/02/09/generation-of-correlated-random-numbers-using-python/

import numpy as np
from numpy.random import RandomState
from scipy.linalg import cholesky    
from scipy.stats import pearsonr
from matplotlib import pyplot as plt

def get_covariance_matrix(num_rows_cols, cov_array):
    assert len(cov_array) == num_rows_cols * (num_rows_cols-1) / 2
    cov_mat = np.eye(num_rows_cols)    
    cov_array_iter = iter(cov_array)
    for i in range(num_rows_cols):
        for j in range(i+1, num_rows_cols):
            cov_mat[i,j] = cov_mat[j,i] = next(cov_array_iter)
    return cov_mat


def generate_correlated_data(num_data_points, num_vars, cov_mat, rs, rowvar=False):    
    """_summary_

    Args:
        num_cars (int): number of joint variables in the time series
        num_data_points (_type_): length of the time series
        cov (_type_): matrix with 0 in lower left triangles, 
            one in diagonale, and correlation coefficient in upper right triangle
            e.g.,
            [
                [1.0, cov],
                [0, 1.0]
            ]
        rs (_type_): _description_

    Returns:
        ndarray: array of shape (2, num_data_points) with give covariance
    """

    # Compute the (upper) Cholesky decomposition matrix
    upper_chol = cholesky(cov_mat)

    # Generate num_vars series of normally distributed (Gaussian) numbers
    rnd = rs.normal(0.0, 1.0, size=(num_data_points, num_vars))

    # Finally, compute the inner product of upper_chol and rnd
    result = rnd @ upper_chol  # shape: (num_data_points, num_vars)

    if rowvar:
        result = np.transpose(result) # shape: (num_vars, num_data_points)
    
    return result

def test_get_correlation_matrix():
    print(get_covariance_matrix(2, [0.5]))
    # print(get_covariance_matrix(3, [0.5, 0.6, 0.7]))


def test_correlation_2(num_data_points=10**5, cov=0.99, seed=None):
    rs = RandomState(seed) # pylint ignore: (undefined-variable)    
    print('2 joint variables')
    cov_mat = get_covariance_matrix(2, [cov])
    # result = generate_correlated_data(num_data_points, num_vars=2, cov_mat=cov_mat, rs=rs)
    result = rs.multivariate_normal(mean=[0]*2, cov=cov_mat, size=num_data_points)
    print(result.shape)
    corr_0_1 , _ = pearsonr(result[:,0], result[:,1])
    print(corr_0_1)
    corr_1_0 , _ = pearsonr(result[:,1], result[:,0])
    print(corr_1_0)
    # print(result)

    fig = plt.figure()
    ax = fig.add_subplot()
    ax.scatter(result[:,0], result[:,1])
    plt.show()

def test_correlation_3(num_data_points=10**5, cov_array=[0.2, 0.4, 0.6], seed=None):
    print('3 joint variables')   
    assert(len(cov_array)==3) 
    rs = RandomState(seed) # pylint ignore: (undefined-variable)    
    cov_mat = get_covariance_matrix(3, cov_array)
    # result = generate_correlated_data(
    #     num_data_points, num_vars=3, cov_mat=cov_mat, rs=rs)
    result = rs.multivariate_normal(mean=[0]*3, cov=cov_mat, size=num_data_points)
    print(result.shape)
    corr_0_1 , _ = pearsonr(result[:,0], result[:,1])
    print(corr_0_1)
    corr_0_2 , _ = pearsonr(result[:,0], result[:,2])
    print(corr_0_2)
    corr_1_2 , _ = pearsonr(result[:,1], result[:,2])
    print(corr_1_2)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(result[:,0], result[:,1], result[:,2])
    plt.show()

if __name__ == "__main__":
    # test_get_correlation_matrix()
    test_correlation_2(cov=0.999, seed=None)
    # test_correlation_3(num_data_points=10**3, cov_array=[0.5, 0.99999, 0.99999], seed=123)