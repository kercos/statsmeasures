import numpy as np
from measures.parametric.gaussian_mi import GaussianMI

def MedianoGamma(X,V,tau=1):
    if not isinstance(V, np.ndarray) or not isinstance(X, np.ndarray):
        raise ValueError("X and V have to be numpy arrays.")
    if V.shape[0] != X.shape[0]:
        raise ValueError("X and V must have the same height.")

    gamma = np.max([GaussianMI(V[0:-tau, :], X[tau:, j]) for j in range(X.shape[1])])

    return gamma