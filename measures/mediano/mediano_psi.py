import numpy as np
from measures.parametric.gaussian_mi import GaussianMI

def MedianoPsi(X,V,tau=1):
    if not isinstance(V, np.ndarray) or not isinstance(X, np.ndarray):
        raise ValueError("X and V have to be numpy arrays.")
    if V.shape[0] != X.shape[0]:
        raise ValueError("X and V must have the same height.")

    v_mi = GaussianMI(V[:-tau], V[tau:])
    x_mi = np.sum([GaussianMI(X[:-tau, j], V[tau:]) for j in range(X.shape[1])])
    psi = v_mi - x_mi

    return psi