from numpy.random import RandomState
from measures.jidt.complexity_tse_kraskov import compute_complexity_tse_kraskov
from measures.parametric.complexity_tse_entropy import compute_complexity_tse_entropy

from measures.utils.jidt import initJVM, shutdownJVM
from measures.utils.utils import random_int
from scipy import stats
from measures.test import dataset

def test_complexity_tse(num_data_points, num_nodes, seed, num_tests):

    initJVM()    

    rs = RandomState(seed)
    seeds = random_int(random_state=rs, size=num_tests)

    funcs = [
        dataset.random_uniform,
        dataset.random_normal,
        dataset.constant_ones,
        dataset.random_normal_constant,
        dataset.random_normal_pairs,
        dataset.paired_correlated_data_09,
        dataset.paired_correlated_data_099999,
        dataset.generalized_correlated_data
    ]

    for f in funcs: 
        tse_param_results, tse_non_param_results = [], []
        for s in seeds:
            rs = RandomState(s)
            data, description = f(num_data_points, num_nodes, rs)
            tse_param_results.append(compute_complexity_tse_entropy(data))
            tse_non_param_results.append(compute_complexity_tse_kraskov(data))
        tse_param_stats = stats.describe(tse_param_results)
        tse_non_param_stats = stats.describe(tse_non_param_results)
        print('\n')
        print(description)
        print(f'Complexity TSE parametric: {tse_param_stats.mean} (~ {tse_param_stats.variance})')
        print(f'Complexity TSE non-param (Kraskov): {tse_non_param_stats.mean} (~ {tse_non_param_stats.variance})')

    shutdownJVM()



if __name__ == "__main__":    
    test_complexity_tse(num_data_points=1000, num_nodes=6, seed=123, num_tests=10)