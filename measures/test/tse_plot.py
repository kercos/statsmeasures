import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt
from scipy.stats import norm
from measures.jidt.complexity_tse_entropy_kozachenko import compute_complexity_tse_entropy_kozachenko
from measures.jidt.entropy_kozachenko import compute_entropy_jidt_kozachenko
from measures.jidt.integration import compute_integration_jidt_kozachenko
from measures.parametric.complexity_tse_entropy import compute_complexity_tse_entropy
from scipy.linalg import toeplitz
from measures.parametric.entropy_via_covariance import compute_entropy_via_coveriance
from measures.parametric.integration_via_covariance import compute_integration_via_covariance
from measures.jidt.complexity_tse_kraskov import compute_complexity_tse_kraskov
from measures.utils.jidt import initJVM
from tqdm import tqdm

from measures.utils.utils import add_noise

def get_toeplitz_cov_matrix(num_nodes, log_sigma, plot=False):
    sigma = np.power(10, log_sigma)
    x_data = range(num_nodes)
    cov_array = norm.pdf(x_data , loc = 0 , scale = sigma)
    cov_array = cov_array / cov_array[0]

    if plot:
        plt.ticklabel_format(useOffset=False)
        plt.plot(cov_array)
        plt.show()

    return toeplitz(cov_array, cov_array)    

def test_toeplitz_cov_matrix(num_nodes, log_sigma_min, log_sigma_max, num_plots):

    log_sigma_array = np.linspace(log_sigma_min, log_sigma_max, num_plots)

    fig = plt.figure()

    for n, log_sigma in enumerate(log_sigma_array, 1):
        ax = fig.add_subplot(1, num_plots, n, projection='3d')
        
        toeplitz_mat = get_toeplitz_cov_matrix(num_nodes, log_sigma, plot=False)
        # print(toeplitz_mat.shape)
        # print(toeplitz_mat)

        x=y=list(range(num_nodes))

        X, Y = np.meshgrid(x, y)  # `plot_surface` expects `x` and `y` data to be 2D
        ax.plot_surface(X, Y, toeplitz_mat)
        ax.set_zlim(0,1)
        ax.set_title(f'Log sigma = {log_sigma:.2g}')
    
    fig.suptitle(f'N = {num_nodes}')

    plt.show()
    
def test_parametric(num_data_points, num_nodes, log_sigma_array, seed=None):    
    c_p, i_p, h_p = [], [], []    
    for log_sigma in tqdm(log_sigma_array):
        cov_mat = get_toeplitz_cov_matrix(num_nodes, log_sigma)
        # print(cov_mat[0,:].min())
        rs = RandomState(seed)
        data = rs.multivariate_normal(mean=[0]*num_nodes, cov=cov_mat, size=num_data_points)
        data = add_noise(data, noise_level=1e-7)
        c_p.append(compute_complexity_tse_entropy(data))
        # i_p.append(-np.log(np.linalg.det(np.cov(data, rowvar=False)))/2)
        i_p.append(compute_integration_via_covariance(data))
        h_p.append(compute_entropy_via_coveriance(data))
        # h_p.append(compute_entropy_jidt_gaussian(data))
        # print(comp)

    h_p_norm = np.array(h_p)
    h_p_norm -= h_p[-1]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(log_sigma_array, c_p, c='blue', label = "complexity")
    ax.plot(log_sigma_array, i_p, c='orange', label = "integration")
    ax.plot(log_sigma_array, h_p, c='green', linestyle='dashed', label = "entropy")
    ax.plot(log_sigma_array, h_p_norm, c='green', label = "entropy (norm)")
    ax.set_xlabel('log_10(sigma)')
    ax.legend()
    plt.show()

def test_non_parametric(num_data_points, num_nodes, log_sigma_array, seed=None):    
    initJVM()
    c_p_kraskov, c_p_kozachenko, i_p, h_p = [], [], [], []
    for log_sigma in tqdm(log_sigma_array):
        cov_mat = get_toeplitz_cov_matrix(num_nodes, log_sigma)
        # print(cov_mat[0,:].min())
        rs = RandomState(seed)
        data = rs.multivariate_normal(mean=[0]*num_nodes, cov=cov_mat, size=num_data_points)
        data = add_noise(data, noise_level=1e-7)
        c_p_kraskov.append(compute_complexity_tse_kraskov(data))
        c_p_kozachenko.append(compute_complexity_tse_entropy_kozachenko(data))
        i_p.append(compute_integration_jidt_kozachenko(data))
        h_p.append(compute_entropy_jidt_kozachenko(data))
        # c_p.append(compute_complexity_tse_entropy_kozachenko(data))

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(log_sigma_array, c_p_kraskov, c='blue', label = "complexity (kraskov)")
    ax.plot(log_sigma_array, c_p_kozachenko, c='red', label = "complexity (kozachenko)")
    ax.plot(log_sigma_array, i_p, c='orange', label = "integration")
    ax.plot(log_sigma_array, h_p, c='green', linestyle='dashed', label = "entropy")
    ax.set_xlabel('log_10(sigma)')
    ax.legend()
    plt.show()
    

if __name__ == "__main__":    
    log_sigma_min = -0.5
    log_sigma_max = 3
    log_sigma_array = np.linspace(log_sigma_min, log_sigma_max, 100)
    # test_toeplitz_cov_matrix(num_nodes=64, log_sigma_min=log_sigma_min, log_sigma_max=log_sigma_max, num_plots=6)
    # test_parametric(num_data_points=1000, num_nodes=10, log_sigma_array=log_sigma_array, seed=123)
    test_non_parametric(num_data_points=1000, num_nodes=10, log_sigma_array=log_sigma_array, seed=123)