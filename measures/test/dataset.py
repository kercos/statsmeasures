import numpy as np
from measures.utils.correlation import get_covariance_matrix
from measures.utils.utils import add_noise

def random_uniform(num_data_points, num_nodes, rs):
    data = rs.uniform(-1, 1, size=(num_data_points, num_nodes))
    description = 'Random Uniform'
    return data, description

def random_normal(num_data_points, num_nodes, rs):
    data = rs.normal(size=(num_data_points, num_nodes))
    description = 'Random Normal Gaussian'
    return data, description

def constant_ones(num_data_points, num_nodes, rs, noise_level = 1e-5):
    data = np.ones((num_data_points, num_nodes))    
    data = add_noise(data, rs, noise_level=noise_level)
    description = 'Constant Ones'
    return data, description

def random_normal_constant(num_data_points, num_nodes, rs, noise_level = 1e-5):
    data = rs.normal(size=(num_data_points, num_nodes))
    for n in range(1, num_nodes):
        data[:,n] = data[:,0]
    data = add_noise(data, rs, noise_level=noise_level)
    description = 'Random Normal Constant'
    return data, description

def random_normal_pairs(num_data_points, num_nodes, rs, noise_level = 1e-5):
    data = rs.normal(size=(num_data_points, num_nodes))
    for n in range(num_nodes//2):
        data[:,n*2+1] = data[:,n*2]
    data = add_noise(data, rs, noise_level=noise_level)
    description = 'Random Normal Pairs'
    return data, description


def paired_correlated_data(num_data_points, num_nodes, rs, cov=0.9):
    assert num_nodes % 2 == 0

    num_pairs = int(num_nodes / 2)

    cov_mat = get_covariance_matrix(2, [cov])
    
    # stack together pairs of coorelated columns
    data = np.column_stack(
        [
            rs.multivariate_normal(mean=[0]*2, cov=cov_mat, size=num_data_points)
            for _ in range(num_pairs)
        ]
    )
    assert data.shape == (num_data_points, num_nodes)    
    description = f'Correlated pairs with cov={cov})'
    return data, description

def paired_correlated_data_09(num_data_points, num_nodes, rs):
    return paired_correlated_data(num_data_points, num_nodes, rs, cov=0.9)

def paired_correlated_data_099999(num_data_points, num_nodes, rs):
    return paired_correlated_data(num_data_points, num_nodes, rs, cov=0.99999)

def generalized_correlated_data(num_data_points, num_nodes, rs, cov_array=None):
    
    if cov_array is None:
        cov_array_length = int(num_nodes * (num_nodes-1) / 2)
        cov_array = np.linspace(0,1,cov_array_length+2)[1:-1]

    cov_mat = get_covariance_matrix(num_nodes, cov_array)
    
    data = rs.multivariate_normal(mean=[0]*num_nodes, cov=cov_mat, size=num_data_points)

    assert data.shape == (num_data_points, num_nodes)    
    description = f'Correlated data with cov_array={cov_array}'
    return data, description