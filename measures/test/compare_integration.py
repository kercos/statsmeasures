from numpy.random import RandomState
from measures.jidt.integration import compute_integration_jidt_gaussian
from measures.parametric.integration import compute_integration_via_covariance
from measures.utils.jidt import initJVM, shutdownJVM
from measures.utils.utils import random_int
from measures.test import dataset
from scipy import stats




def test_complexity_tse(num_data_points, num_nodes, seed, num_tests):

    initJVM()    

    rs = RandomState(seed)
    seeds = random_int(random_state=rs, size=num_tests)

    funcs = [
        dataset.random_uniform,
        dataset.random_normal,
        dataset.constant_ones,
        dataset.random_normal_constant,
        dataset.random_normal_pairs,
        dataset.paired_correlated_data_09,
        dataset.paired_correlated_data_099999,
        dataset.generalized_correlated_data
    ]

    for f in funcs: 
        intg_cov_results, intg_jidt_results = [], []
        for s in seeds:
            rs = RandomState(s)
            data, description = f(num_data_points, num_nodes, rs)
            intg_cov_results.append(compute_integration_via_covariance(data))
            intg_jidt_results.append(compute_integration_jidt_gaussian(data))
        intg_cov_stats = stats.describe(intg_cov_results)
        intg_jidt_stats = stats.describe(intg_jidt_results)
        print('\n')
        print(description)
        print(f'Integration via covariance: {intg_cov_stats.mean} (~ {intg_cov_stats.variance})')
        print(f'Integration jidt gaussian:  {intg_jidt_stats.mean} (~ {intg_jidt_stats.variance})')

    shutdownJVM()



if __name__ == "__main__":    
    test_complexity_tse(num_data_points=1000, num_nodes=6, seed=123, num_tests=10)