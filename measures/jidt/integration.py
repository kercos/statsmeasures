import numpy as np
from measures.jidt.entropy_gaussian import compute_entropy_jidt_gaussian
from measures.jidt.entropy_kernel import compute_entropy_jidt_kernel
from measures.jidt.entropy_kozachenko import compute_entropy_jidt_kozachenko
from measures.utils.jidt import initJVM, shutdownJVM
from numpy.random import RandomState

# also called total correlation

def compute_integration_jidt_gaussian(data):
    # TSE
    assert data.ndim == 2
    num_rows, num_cols = data.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    sum_individual_entropies = 0
    for j in range(num_cols):
       column_data = np.expand_dims(data[:, j], -1)
       sum_individual_entropies += compute_entropy_jidt_gaussian(column_data)

    overall_entropy = compute_entropy_jidt_gaussian(data)    
    return sum_individual_entropies - overall_entropy

def compute_integration_jidt_kernel(data):
    # TSE
    assert data.ndim == 2
    num_rows, num_cols = data.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    sum_individual_entropies = 0
    for j in range(num_cols):
       column_data = np.expand_dims(data[:, j], -1)
       sum_individual_entropies += compute_entropy_jidt_kernel(column_data)

    overall_entropy = compute_entropy_jidt_kernel(data)    
    return sum_individual_entropies - overall_entropy

def compute_integration_jidt_kozachenko(data):
    # TSE
    assert data.ndim == 2
    num_rows, num_cols = data.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    sum_individual_entropies = 0
    for j in range(num_cols):
       column_data = np.expand_dims(data[:, j], -1)
       sum_individual_entropies += compute_entropy_jidt_kozachenko(column_data)

    overall_entropy = compute_entropy_jidt_kozachenko(data)    
    return sum_individual_entropies - overall_entropy

def test_compute_integration_jidt(seed=None, mi_type='gaussian'):    
    rs = RandomState(seed)
    data = rs.random_sample((500, 4)) 
    if mi_type == 'gaussian':   
        mi = compute_integration_jidt_gaussian(data)    
    elif mi_type == 'kernel':
        mi = compute_integration_jidt_kernel(data)    
    elif mi_type == 'kozachenko':
        mi = compute_integration_jidt_kozachenko(data)    
    else:
        assert False, "mi_type must be in ['gaussian', 'kernel', 'kozachenko']"
    print(mi)    

if __name__ == "__main__":
    initJVM()
    test_compute_integration_jidt(123, mi_type='gaussian')
    test_compute_integration_jidt(123, mi_type='kernel')
    test_compute_integration_jidt(123, mi_type='kozachenko')
    shutdownJVM()
    