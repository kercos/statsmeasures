"""
Implements TSE complexity.
"""
import numpy as np
from numpy.random import RandomState
from measures.jidt.entropy_kozachenko import compute_entropy_jidt_kozachenko
from measures.utils.bipartitions import compute_bipartitions
from measures.test import dataset
from measures.utils.jidt import initJVM

def compute_complexity_tse_entropy_kozachenko(data):
    """(called in the past neural_complexity)

    Args:
        data (_type_): _description_
        rs (_type_, optional): _description_. Defaults to None.

    Returns:
        _type_: _description_
    """
    assert data.ndim == 2
    num_datapoint, num_nodes = data.shape
    assert num_datapoint > num_nodes
    h_AB = compute_entropy_jidt_kozachenko(data)
    # print('entropy AB: ', h_AB)
    bipart_list = compute_bipartitions(num_nodes, sort_by_size=True)  
    # all possible bipartitions of indexes, sorted by cardinality (left <= right)

    # keep track of mi values of each group indexed by k-1
    # (where k is the size of smallest bipartition set)
    mi_group_size = int(num_nodes / 2)
    mi_groups_val = np.zeros(mi_group_size)
    mi_groups_len = np.zeros(mi_group_size)
    for bipart_left, bipart_right in bipart_list:
        A = data[:, bipart_left]   # columns specified in indexes of first bipartition set
        B = data[:, bipart_right]  # columns specified in indexes of second bipartition set
        k = len(bipart_left) # left bipartition cardinality always <= right bipartition                
        h_A = compute_entropy_jidt_kozachenko(A)
        h_B = compute_entropy_jidt_kozachenko(B)
        mi = h_A + h_B - h_AB
        mi_groups_val[k - 1] += mi
        mi_groups_len[k - 1] += 1
    neural_complexity = np.sum(mi_groups_val / mi_groups_len)  # sum over averaged values
    return neural_complexity


def test_compute_complexity_tse_entropy_kozachenko(seed=None):
    initJVM()
    rs = RandomState(seed)
    # data, _ = dataset.random_normal(500, 4, rs)
    # data, _ = dataset.random_normal_constant(1000, 6, rs)
    # data = rs.random_sample((500, 4))    
    data, _ = dataset.random_normal_pairs(1000, 6, rs)
    mi = compute_complexity_tse_entropy_kozachenko(data)    
    print(mi)


if __name__ == "__main__":
    test_compute_complexity_tse_entropy_kozachenko(123)
