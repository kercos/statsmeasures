import numpy as np
from measures.utils.bipartitions import compute_bipartitions
from measures.utils.jidt import initJVM, shutdownJVM
from measures.jidt.mi_kraskov import compute_mi_kraskov
from numpy.random import RandomState
from measures.test import dataset

def compute_complexity_tse_kraskov(data, add_noise=False):
    # TSE
    assert data.ndim == 2
    num_rows, num_cols = data.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    bp = compute_bipartitions(num_cols, sort_by_size=True)
    mi_group_size = num_cols // 2
    mi_groups_val = np.zeros(mi_group_size)
    mi_groups_len = np.zeros(mi_group_size)

    for subset_a, subset_b in bp:
        sub_data_a = data[:,subset_a] # column indexes specified in subset_a
        sub_data_b = data[:,subset_b] # column indexes specified in subset_b
        k = len(subset_a)
        mi_pair = compute_mi_kraskov(sub_data_a, sub_data_b, add_noise=add_noise)
        mi_groups_val[k - 1] += mi_pair
        mi_groups_len[k - 1] += 1

    mi = np.sum(mi_groups_val / mi_groups_len)
    return mi

def test_compute_complexity_tse_kraskov(seed=None):
    initJVM()
    rs = RandomState(seed)
    # data = rs.random_sample((500, 4))    
    # data, _ = dataset.random_normal_constant(1000, 4, rs)
    data, _ = dataset.random_normal_pairs(1000, 4, rs)
    mi = compute_complexity_tse_kraskov(data)    
    print(mi)
    shutdownJVM()

if __name__ == "__main__":
    test_compute_complexity_tse_kraskov(123)