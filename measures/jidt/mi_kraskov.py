import jpype as jp
import numpy as np
from measures.test import dataset
from measures.utils.jidt import initJVM, shutdownJVM
from numpy.random import RandomState

def compute_mi_kraskov(data1, data2, add_noise=False):
    # multi variate mutual information
    jp_kraskov_pkg = jp.JPackage("infodynamics.measures.continuous.kraskov")
    mi_calc = jp_kraskov_pkg.MutualInfoCalculatorMultiVariateKraskov1()
    
    if not add_noise:
        mi_calc.setProperty("NOISE_LEVEL_TO_ADD", "0") # no noise for reproducibility
    
    mi_calc.initialise(data1.shape[1], data2.shape[1]) # number of source and destination joint variables
    
    mi_calc.setObservations(
        jp.JArray(jp.JDouble, 2)(data1), 
        jp.JArray(jp.JDouble, 2)(data2)
    )
    
    result = mi_calc.computeAverageLocalOfObservations()
    return result

def test_compute_mi():
    initJVM()
    data1 = np.random.random_sample((1000, 10))    
    # mi = compute_mi(data1, data1)    
    data2 = np.random.random_sample((1000, 10))
    # data2 = data1 + 0.5 * np.random.random_sample((1000, 10))
    mi = compute_mi_kraskov(data1, data2)    
    print(mi)
    shutdownJVM()

def test_mi_correlation():
    initJVM()
    data1, _ = dataset.paired_correlated_data(num_data_points=100, num_nodes=2, rs=RandomState(123), cov=0.9)
    data2 = np.copy(data1)
    # print(data.shape)
    mi = compute_mi_kraskov(data1, data2)
    print(mi)
    shutdownJVM()

if __name__ == "__main__":
    # test_compute_mi()
    test_mi_correlation()