import numpy as np
from measures.utils.power_set import powerset
from measures.utils.jidt import initJVM, shutdownJVM
from measures.jidt.mi_kraskov import compute_mi_kraskov

def compute_mi_powerset(data1, data2):
    assert data1.shape == data2.shape
    assert data1.ndim == 2
    num_rows, num_cols = data1.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    power_set_idx = powerset(num_cols, remove_empty=True)
    power_set_size = len(power_set_idx)
    mi_matrix = np.zeros((power_set_size, power_set_size))

    for i, ps_a in enumerate(power_set_idx):
        for j, ps_b in enumerate(power_set_idx):            
            sub_matrix_a = data1[:,ps_a] # column indexes specified in ps_a
            sub_matrix_b = data2[:,ps_b] # column indexes specified in ps_b
            mi_matrix[i][j] = compute_mi_kraskov(sub_matrix_a, sub_matrix_b)

    mi = mi_matrix.mean()
    return mi

def test_compute_mi_powerset():
    initJVM()
    data1 = np.random.random_sample((500, 4))    
    mi = compute_mi_powerset(data1, data1)    
    # data2 = np.random.random_sample((500, 4))
    # data2 = data1 + 0.5 * np.random.random_sample((500, 4))
    # mi = compute_mi_powerset(data1, data2)    
    print(mi)
    shutdownJVM()

if __name__ == "__main__":
    test_compute_mi_powerset()