import numpy as np
from scipy.stats import gaussian_kde
import numpy.random as rn

def GaussianMI(X, Y):
    """
    Gaussian mutual information between variables X and Y.
    
    Parameters:
    X -- A matrix of size [T, Dx]
    Y -- A matrix of size [T, Dy]
    
    Returns:
    mi -- Mutual information between X and Y
    
    Raises:
    ValueError -- If X and Y are not matrices of the same height
    """
    # Parameter checks
    if np.ndim(X) == 1:
        X = np.reshape(X, (-1, 1))
    if np.ndim(Y) == 1:
        Y = np.reshape(Y, (-1, 1))
    if not (np.ndim(X) == 2 and np.ndim(Y) == 2 and X.shape[0] == Y.shape[0]):
        raise ValueError("X and Y must be matrices of the same height.")
    Dx = X.shape[1]
    Dy = Y.shape[1]

    # Compute correlation and MI
    rho = np.corrcoef(np.hstack((X, Y)).T)
    mi = 0.5*(np.linalg.slogdet(rho[0:Dx, 0:Dx])[1] + 
              np.linalg.slogdet(rho[Dx:, Dx:])[1] - 
              np.linalg.slogdet(rho)[1])
    return mi


def test():
    rn.seed(123)
    X = rn.randn(100,2)
    V = rn.randn(100,1)
    mi = GaussianMI(X,V)

    print(mi)


if __name__ == "__main__":
    test()