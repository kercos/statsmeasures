import numpy as np
from numpy.random import RandomState
from measures.parametric.entropy_via_covariance import compute_entropy_via_coveriance
from measures.test import dataset

def compute_mi(A,B):
    AB = np.column_stack([A,B])
    h_A = compute_entropy_via_coveriance(A)
    h_B = compute_entropy_via_coveriance(B)
    h_AB = compute_entropy_via_coveriance(AB)
    return h_A + h_B - h_AB


def test():
    rs = RandomState()
    # data, _ = dataset.random_normal_constant(1000, 4, rs)
    data, _ = dataset.random_normal_pairs(1000, 4, rs)
    A = data[:,0]
    B = data[:,1:]
    mi = compute_mi(A, B)
    print(mi)

if __name__ == "__main__":
    # test_mutual_information(seed=0, num_data_points = 10000)
    test()
