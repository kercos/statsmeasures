import numpy as np
from measures.jidt.entropy_gaussian import compute_entropy_jidt_gaussian
from measures.parametric.entropy_via_covariance import compute_entropy_via_coveriance
from measures.utils.jidt import initJVM, shutdownJVM
from numpy.random import RandomState

# also called total correlation

def compute_integration_via_covariance(data):
    # TSE
    assert data.ndim == 2
    num_rows, num_cols = data.shape
    assert num_rows > num_cols # rows is time series, cols are variables
    sum_individual_entropies = 0
    for j in range(num_cols):
       column_data = np.expand_dims(data[:, j], -1)
       sum_individual_entropies += compute_entropy_via_coveriance(column_data)

    overall_entropy = compute_entropy_via_coveriance(data)    
    return sum_individual_entropies - overall_entropy

def test_compute_integration_via_covariance(seed=None):
    initJVM()
    rs = RandomState(seed)
    data = rs.random_sample((500, 4))    
    mi = compute_integration_via_covariance(data)    
    print(mi)
    shutdownJVM()

if __name__ == "__main__":
    test_compute_integration_via_covariance(123)