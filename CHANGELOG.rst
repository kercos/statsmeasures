=========
Changelog
=========

Version 0.6.0
=============
- Mediano Delta and Gamma (octave and python)

Version 0.5.0
=============
- Mediano PSI (octave and python)
- Gaussian MI
- refactoring

Version 0.4.0
=============
- fixing import libraries
- improving tests

Version 0.3.0
=============
- better packaging
- including infodynamics.jar

Version 0.2.1
=============
- tranfer entropy

Version 0.2.0
=============
- major refactoring
- jidt subpackage
- more testing on measures

Version 0.1.0
=============
- major refactoring

Version 0.0.2
=============
- overall cond MI

Version 0.0.1
=============
- initial build of the package

